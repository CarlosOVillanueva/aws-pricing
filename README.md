# aws-pricing
## Summary
The aws-pricing module is a PHP class that pulls the updated price catalog from AWS.

## Requirements
- Script will pull updated price list from AWS either adhoc or via CRON
- Price list will contain Region, Instance Size, Reserve Hourly Cost (1yr term) and On Demand Hourly Cost
- Price list will only contain linux AWS instance pricing
- Price list will be in a delimited format that can easily be imported into any external system (i.e., CSV, Tab, JSON, or Array)
- Script will return either a php object, stdout or file system object (timestamped) 

## Class
The aws-pricing class is located under /lib/aws-pricing.php. The following is a quick summary of the parameters and methods/functions defined within the class.

	# @param:
	# 	JSON - Boolean, returns test() result as JSON; otherwise PHP array
	#	tab - Boolean, returns getDelimited() as tabbed text; otherwise CSV
	#	debug - Prints test() and getDelimited() data on screen
	#	pricingrawJSON - String, raw JSON data from amazon
	#	pricingPHP - String, pricing as PHP array
	#	pricingJSON - String, pricing as JSON 
	#	fso - Boolean, writes file with getDelimited() results
	# @method:
	#	get() - Pulls CSV files from AWS and sets raw pricing to contents of linux-unix-shared
	#	set() - Sets pricingPHP and pricingJSON
	#	test() - Tests data pulled from AWS
	#	getDelimited() - Returns AWS pricing in either CSV or Tab Delimited format

The class pulls all the pricing from AWS; however, it only uses the linux-unix-shared.csv file. This can be modified at a later time to support pricing from other instance types. The class supports several boolean options - to include, returning raw JSON data from the downloaded csv file, formatted JSON and PHP array objects, or a file system object, containing either a CSV or Tab delimited file.

## Binaries
All binaries are written in PHP, but are executable via the command-line.

### getDelimited
The script will pull a comma delimited AWS price list and return a date-stamped file (e.g., <TIME>_price.txt). This file can be imported into Excel or any application that parses CSV files. 
### getTest
The script will pull the current AWS price list and return the PHP array of all the objects defined in the price list. One can utilize this output to either modify the class file or debug changes to the CSV file. One can also modify the binary to output JSON vs PHP objects. http://json2table.com/ offers a nifty web-based tool that will conver the JSON output to a table.
