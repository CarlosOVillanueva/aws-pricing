<?php
###################################################
# @name: aws_pricing 
# @author: Carlos Villanueva
# @description: Utility to pull pricing from AWS
# @param:
# 	JSON - Boolean, returns test() result as JSON; otherwise PHP array
#	tab - Boolean, returns getDelimited() as tabbed text; otherwise CSV
#	debug - Prints test() and getDelimited() data on screen
#	pricingrawJSON - String, raw JSON data from amazon
#	pricingPHP - String, pricing as PHP array
#	pricingJSON - String, pricing as JSON 
#	fso - Boolean, writes file with getDelimited() results
# @method:
#	get() - Pulls CSV files from AWS and sets raw pricing to contents of linux-unix-shared
#	set() - Sets pricingPHP and pricingJSON
#	test() - Tests data pulled from AWS
#	getDelimited() - Returns AWS pricing in either CSV or Tab Delimited format
###################################################
class aws_pricing
{
	public $JSON = false;
	public $tab = false;
	public $debug = false;
	public $fso = false;
	public $pricingrawJSON = null;
	public $pricingPHP = null;
	public $pricingJSON = null;

	function get()
	{
		#Get Pricing from AWS
		$sh_getaws = 'rm -f *.js;for i in $(curl http://aws.amazon.com/ec2/pricing/ 2>/dev/null | grep \'model:\' | sed -e "s/.*\'\\(.*\\)\'.*/http:\\\\1/");do wget --quiet $i;done';
		shell_exec($sh_getaws);

		#Get JSON data from downloaded file
		$this -> pricingrawJSON = shell_exec("cat linux-unix-shared.min.js");

		#Purge downloaded JSON files 
		shell_exec("rm -f *.js");

		return $this -> pricingrawJSON;
	}

	function set()
	{
		$this -> get();

		#Sanitize AWS JSON
		$pricingJSON = preg_replace("/^.*\n.*\n.*\n.*\n.*\ncallback\(|\)\;$/i","",$this -> pricingrawJSON);
		$pricingJSON = preg_replace("/([a-zA-Z]+?)\:/i",'"$1":',$pricingJSON);

		#Convert AWS JSON to PHP Array
		$pricingPHP = json_decode($pricingJSON);

		#Remove unnessary nodes/objects from PHP array
		unset($pricingPHP -> config -> valueColumns);
		unset($pricingPHP -> config -> currencies);
		unset($pricingPHP -> vers);

		#Set pricelist
		$this -> pricingPHP = $pricingPHP -> config -> regions;
		$this -> pricingJSON = json_encode($this -> pricingPHP);

		return $this -> pricingPHP;
	}

	function test()
	{
		$this -> set();

		if ($this -> JSON === false)
		{
			$test = $this -> pricingPHP;
		}
		else
		{
			$test = $this -> pricingJSON;
		}

		if ($this -> debug === true)
		{
			print_r($test);
		}

		return $test;
	}

	function getDelimited()
	{
		$this -> set();

		#Region hash
		$regions[] = array();
		$regions["ap-northeast-1"] = "Asia Pacific (Tokyo)";
		$regions["ap-southeast-1"] = "Asia Pacific (Singapore)";
		$regions["ap-southeast-2"] = "Asia Pacific (Sydney)";
		$regions["eu-central-1"] = "EU (Frankfurt)";
		$regions["eu-west-1"] = "EU (Ireland)";
		$regions["sa-east-1"] = "South America (Sao Paulo)";
		$regions["us-east-1"] = "US East (N. Virginia)";
		$regions["us-west-1"] = "US West (N. California)";
		$regions["us-gov-west-1"] = "US West (N. California) Federal";
		$regions["us-west-2"] = "US West (Oregon)";

		#Set delimiter
		if ($this -> tab === true)
		{
			$delimiter = "\t";
		}
		else
		{
			$delimiter = ",";
		}

		#Set header
		$data = "Region".$delimiter."Size".$delimiter."On Demand".$delimiter."No Upfront"."\n";
		
		#Get data
		foreach($this -> pricingPHP as $p  => $pn) {
			$region = $pn -> region;
			
			#Test if region exist
			if (!$regions[$region])
			{
				continue;
			}

			$instanceType = $pn -> instanceTypes;
			
			foreach($instanceType as $i => $in ) {
				$type = $in -> type;
				$terms = $in -> terms;
				
				foreach($terms as $t => $tn) {
					$term = $tn -> term;
					
					if ($term == "yrTerm1")
					{
						#Ondemand	
						$onDemandHourly = $tn -> onDemandHourly;
						$od = $onDemandHourly[0] -> prices -> USD;
						#Purchase Options
						$purchaseOptions = $tn -> purchaseOptions;
						foreach($purchaseOptions as $p => $pn){
							if ($pn -> purchaseOption == "noUpfront") {
								$nuf = $pn -> valueColumns[2] -> prices -> USD;
							}
						}
						#Set data
						$data.= $regions[$region].$delimiter.$type.$delimiter.$od.$delimiter.$nuf."\n";
					}
				}
			}
		}
		
		if ($this -> debug === true)
		{
			print $data;
		}
		
		if ($this -> fso === true)
		{
			$date = time();
			shell_exec('echo "'.$data.'" > '.$date.'_price.txt');
		}

		return $data;
	}

}
?>
